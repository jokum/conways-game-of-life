#ifndef GAMEOFLIFE_H
#define GAMEOFLIFE_H

#include <vector>
#include <cassert>
#include <fstream>
#include <random>

namespace gol
{
    class GameOfLife
    {
    public:
        typedef std::vector<bool> row_t;
        typedef std::vector<row_t> board_t;

        GameOfLife(int rows = 6, int cols = 6)
            : started(false),
            num_alive(0),
            num_steps(0),
            num_rows(rows),
            num_cols(cols),
            front(num_rows, row_t(num_cols)),
            back(num_rows, row_t(num_cols)),
            front_view(&front), back_view(&back)
        { }
        GameOfLife(int rows, int cols, const std::bernoulli_distribution& bern, std::mt19937 &rng);
        GameOfLife(std::ifstream &ifs);

        void alive(int row, int col, bool is_alive);
        bool alive(int row, int col) const;
        void start();
        void step_begin();
        void step_end();

        inline int living_count() const { return num_alive; }
        inline int steps() const { return num_steps; }
        inline int rows() const { return num_rows; }
        inline int cols() const { return num_cols; }

    private:
        const static char comment_mark = ';';
        const static char live_cell = 'o';

        void update(int row, int col);
        void copy_back();
        void check_bounds(int row, int col) const;

        bool started;
        int num_alive;
        int num_steps;
        int num_rows;
        int num_cols;
        board_t front, back;
        board_t *front_view, *back_view;
    };
}
#endif // Include guard.