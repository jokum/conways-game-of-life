#ifndef GOLCLI_H
#define GOLCLI_H

#ifndef GAMEOFLIFE_H
#include "GameOfLife.h"
#endif

namespace gol
{
    class GoLCLI
    {
    public:
        GoLCLI(const GameOfLife &golref, int step_delay = 500)
            : gol(golref), delay(step_delay) {}

        void run();
        inline void alive(int row, int col, bool is_alive) { gol.alive(row, col, is_alive); }
        inline bool alive(int row, int col) { return gol.alive(row, col); }
        inline int rows() const { return gol.rows(); }
        inline int cols() const { return gol.cols(); }

        const static unsigned char alive_char = 'O',
            dead_char = static_cast<const unsigned char>(250); // Centered bullet.

    private:
        GameOfLife gol;
        int delay;
    };
}
#endif // Include guard.