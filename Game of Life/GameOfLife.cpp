#ifndef GAMEOFLIFE_H
#include "GameOfLife.h"
#endif

#include <stdexcept>
#include <string>

using std::string;

namespace gol
{
    GameOfLife::GameOfLife(int rows, int cols, const std::bernoulli_distribution& bern, std::mt19937 &rng)
        : started(false),
        num_alive(0),
        num_steps(0),
        num_rows(rows),
        num_cols(cols),
        front(num_rows, row_t(num_cols)),
        back(num_rows, row_t(num_cols)),
        front_view(&front), back_view(&back)
    {
        for (int i = 0; i < rows; ++i)
        {
            for (int j = 0; j < cols; ++j)
            {
                alive(i, j, bern(rng));
            }
        }
    }

    GameOfLife::GameOfLife(std::ifstream &ifs)
        : started(false),
        num_alive(0),
        num_steps(0),
        num_rows(0),
        num_cols(0),
        front(),
        back(),
        front_view(&front), back_view(&back)
    {
        string s;
        while (std::getline(ifs, s))
        {
            /*
            The first line decides the column count. Any rows of different
            length are trimmed or false-filled:
            ...o.[EOL]      => 00010
            .....[EOL]      => 00000
            ..o[EOL]        => 00100
            .......o[EOL]   => 00000
            */
            int len = s.size();
            if (len && s[0] == GameOfLife::comment_mark) { continue; }

            if (num_cols == 0) { num_cols = len; }

            row_t new_row(num_cols, false);
            front.push_back(new_row);
            back.emplace_back(std::move(new_row));
            ++num_rows;

            for (int i = 0; i < len && i < num_cols; ++i)
            {
                if (s[i] == GameOfLife::live_cell)
                {
                    alive(num_rows - 1, i, true);
                }
            }
        }
    }

    void GameOfLife::copy_back()
    {
        assert(started);
        for (int i = 0; i < rows(); ++i)
        {
            (*back_view)[i] = (*front_view)[i];
        }
    }

    void GameOfLife::update(int row, int col)
    {
        assert(started);
        int top     = (rows() + row - 1) % rows(),  //    left j right
            bot     = (rows() + row + 1) % rows(),  // top  1  1  0  = 2
            left    = (cols() + col - 1) % cols(),  //   i  1  x  0  = 1
            right   = (cols() + col + 1) % cols(),  // bot  0  0  0  = 0

            neighbours =
            (*front_view)[top][left]    +
            (*front_view)[top][col]     +
            (*front_view)[top][right]   +
            (*front_view)[row][left]    +
            // (*front_view)[row][col] (i.e. self) is not a neighbour.
            (*front_view)[row][right]   +
            (*front_view)[bot][left]    +
            (*front_view)[bot][col]     +
            (*front_view)[bot][right];

        if ((neighbours < 2 || neighbours > 3) && alive(row, col))
        {
            alive(row, col, false);
        }
        else if (neighbours == 3 && !alive(row, col))
        {
            alive(row, col, true);
        }
    }

    void GameOfLife::start()
    {
        if (!started)
        {
            using std::swap;
            started = true;
            /*
            GameOfLife::alive(int, int, bool) is used to seed the board but it
            always assigns to back_view. Thus these two need to be swapped before the
            first run so we're always working with front_view.
            */
            swap(front_view, back_view);
        }
    }

    void GameOfLife::step_begin()
    {
        assert(started);
        copy_back();
    }

    void GameOfLife::step_end()
    {
        assert(started);
        using std::swap;

        for (int i = 0; i < rows(); ++i)
        {
            for (int j = 0; j < cols(); ++j)
            {
                update(i, j);
            }
        }

        ++num_steps;
        swap(front_view, back_view);
    }

    void GameOfLife::alive(int row, int col, bool is_alive)
    {
        check_bounds(row, col);

        if (is_alive && !(*back_view)[row][col]) { ++num_alive; }
        else if (!is_alive && (*back_view)[row][col]) { --num_alive; }

        (*back_view)[row][col] = is_alive;
    }

    bool GameOfLife::alive(int row, int col) const
    {
        check_bounds(row, col);
        return (*back_view)[row][col];
    }

    void GameOfLife::check_bounds(int row, int col) const
    {
        if (row < 0 || col < 0) {
            string s("negative row or col: " + row);
            s.append("," + col);
            throw std::out_of_range(s);
        } else if (row >= rows() || col >= cols()) {
            string s("row or col out of range: " + row);
            s.append("," + col);
            throw std::out_of_range(s);
        }
    }
}