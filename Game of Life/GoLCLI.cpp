#ifndef GOLCLI_H
#include "GoLCLI.h"
#endif

#include <Windows.h>
#undef max
#undef min
#include <iostream>
#include <limits>

using std::cout;

namespace gol
{
    void GoLCLI::run()
    {
        gol.start();

        while (gol.living_count() > 0 && gol.steps() < std::numeric_limits<int>::max())
        {
            gol.step_begin();

            cout << "Step " << gol.steps() << ", living cells: " << gol.living_count() << "\n";
            for (int i = 0; i < gol.rows(); ++i)
            {
                for (int j = 0; j < gol.cols(); ++j)
                {
                    cout << (gol.alive(i, j) ? GoLCLI::alive_char : GoLCLI::dead_char);
                }
                cout << '\n';
            }
            cout << '\n';
            cout.flush();

            gol.step_end();
            Sleep(delay);
        }

        cout << "Cycle ended after " << gol.steps() << " steps." << std::endl;
        std::cin.get();
    }
}