#ifndef GOLCLI_H
#include "GoLCLI.h"
#endif

#include <ctime>

static std::mt19937 rng(static_cast<unsigned long>(time(0)));
static std::bernoulli_distribution bernoulli(0.1);

using gol::GameOfLife;
using gol::GoLCLI;

int main(int argc, const char* argv[])
{
    if (argc == 2)
    {
        auto file = std::ifstream(argv[1]);
        GameOfLife gol(file);
        GoLCLI cli(gol);
        cli.run();
    }
    else
    {
        GameOfLife gol(10, 20, bernoulli, rng);
        GoLCLI cli(gol);
        cli.run();
    }

    return 0;
}